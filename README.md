# APU Feature Extension Card for the UltraZohm's CarrierBoard/BaseSystem #

This PCB will add some crucial hardware peripherals to the existing systems by means of an extension board to be mounted within the chassis (but not using any of the A/D slots).

### Feature List/Requests/Issues ###

* Low-level configuration storage: Cf. [Issue #124 in the UltraZohm CarrierBoard repo](https://bitbucket.org/ultrazohm/ultrazohm_carrierboard/issues/124/low-level-configuration-storage-ethernet)
* Probably additional user interfaces (buttons/LEDs)
* High-speed storage for OS, application and logs: Cf. [Issue #2 in this repository](https://bitbucket.org/ultrazohm/uz_per_rtc_mac/issues/2/ssd-integration-options)

### How do I get set up? / Contribution guidelines / Who do I talk to? ###

* Eyke and Martin
* Feel free to open an issue here